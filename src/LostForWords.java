import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Game written for COMP 2210 assignment 6. Uses the interface WordSearchGame that was provided for the project.
 * loadLexicon() must be called before any method the that relies on the lexicon is called.
 *
 * @author Matthew Cather
 * @version 11/2/15
 */
public class LostForWords implements WordSearchGame {
   /**
    * Contains the dictionary of valid words.
    */
   private TreeSet<String> lexicon;

   /**
    * The check for the loaded lexicon.
    */
   private boolean lexiconLoaded;

   /**
    * The Board contains and organized all of the cells.
    */
   private Board board;

   /**
    * Creates the default board and flags the lexicon as not loaded.
    */
   public LostForWords() {
      board = new Board();
      lexiconLoaded = false;
   }

   @Override
   public void loadLexicon(String fileName) {
      if (fileName == null) {
         throw new IllegalArgumentException();
      }
      try {
         Scanner lineScanner = new Scanner(new File(fileName));
         lexicon = new TreeSet<String>();
         while (lineScanner.hasNext()) {
            String temp = lineScanner.nextLine();
            lexicon.add(new Scanner(temp).next().toLowerCase());
         }
         lexiconLoaded = true;
      } catch (FileNotFoundException e) {
         throw new IllegalArgumentException();
      }
   }

   @Override
   public void setBoard(String[] letterArray) {
      board = new Board(letterArray);
   }

   @Override
   public String getBoard() {
      return board.toString();
   }

   @Override
   public SortedSet<String> getAllValidWords(int minimumWordLength) {
      if (minimumWordLength < 1) throw new IllegalArgumentException("Minimum word length must be grater that 1");
      if (!lexiconLoaded) throw new IllegalStateException("Must load lexicon!");
      LinkedList<Cell> visited = new LinkedList<Cell>();
      SortedSet<String> output = new TreeSet<String>();
      for (int x = 0; x < board.getSideLength(); x++) {
         for (int y = 0; y < board.getSideLength(); y++) {
            visited.add(board.getCell(x,y));
            allValidWords_recursive(visited,minimumWordLength, output);
            visited.clear();
         }
      }
      return output;
   }

   private void allValidWords_recursive(LinkedList<Cell> visited,int minimumWordLength, SortedSet<String> found) {
      String word = getWord(visited);
      if (word.length() >= minimumWordLength && isValidWord(word.toLowerCase())) found.add(word);
      if (isValidPrefix(word)) {
         LinkedList<Cell> neighbors = board.getNeighbors(visited.getLast());
         neighbors.removeAll(visited);
         for (Cell neighbor : neighbors) {
            visited.add(neighbor);
            allValidWords_recursive(visited, minimumWordLength, found);
         }
      }
      visited.removeLast();
   }

   @Override
   public int getScoreForWords(SortedSet<String> words, int minimumWordLength) {
      if (minimumWordLength < 1) throw new IllegalArgumentException("Minimum word length must be grater that 1");
      if (!lexiconLoaded) throw new IllegalStateException("Must load lexicon!");
      int output = 0;
      for (String s : words) {
         if (s.length() >= minimumWordLength && isValidWord(s) && !isOnBoard(s).isEmpty()) {
            output += s.length() - minimumWordLength + 1;
         }
      }
      return output;
   }

   @Override
   public boolean isValidWord(String wordToCheck) {
      if (wordToCheck == null) throw new IllegalArgumentException("Word must not be null");
      if (!lexiconLoaded) throw new IllegalStateException("Must load lexicon!");
      return lexicon.contains(wordToCheck.toLowerCase());
   }

   @Override
   public boolean isValidPrefix(String prefixToCheck) {
      if (!lexiconLoaded) throw new IllegalStateException("Must load lexicon!");
      String temp = lexicon.ceiling(prefixToCheck.toLowerCase());
      return temp.startsWith(prefixToCheck.toLowerCase());
   }

   @Override
   public List<Integer> isOnBoard(String wordToCheck) {
      if (wordToCheck == null) throw new IllegalArgumentException("Word must not be null");
      if (!lexiconLoaded) throw new IllegalStateException("Must load lexicon!");
      LinkedList<Cell> visited = new LinkedList<Cell>();
      List<Integer> output = new LinkedList<Integer>();
      for (int x = 0; x < board.getSideLength(); x++) {
         for (int y = 0; y < board.getSideLength(); y++) {
            visited.add(board.getCell(x,y));
            if (isOnBoard_recursive(visited, wordToCheck.toLowerCase())) {
               for (Cell c : visited) {
                  output.add(board.getSideLength() * c.getX() + c.getY());
               }
               return output;
            }
         }
      }
      return output;
   }

   private boolean isOnBoard_recursive(LinkedList<Cell> visited, String wordToFind) {
      String word = getWord(visited).toLowerCase();
      if (wordToFind.equals(word)) return true;
      if (wordToFind.startsWith(word)) {
         LinkedList<Cell> neighbors = board.getNeighbors(visited.getLast());
         neighbors.removeAll(visited);
         for (Cell neighbor : neighbors) {
            visited.add(neighbor);
            if (isOnBoard_recursive(visited, wordToFind)) return true;
         }
      }
      visited.removeLast();
      return false;
   }

   private String getWord(List<Cell> list) {
      StringBuilder temp = new StringBuilder();
      for (Cell c : list) {
         temp.append(c.getValue());
      }
      return temp.toString();
   }
}
