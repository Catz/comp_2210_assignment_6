public class Cell implements Comparable<Cell>{
   private int x;
   private int y;
   String value;

   public Cell(int _x, int _y, String _value) {
      x = _x;
      y = _y;
      value = _value;
   }

   public int getX() {
      return x;
   }

   public int getY() {
      return y;
   }

   public String getValue() {
      return value;
   }

   @Override
   public int compareTo(Cell o) {
      if (x > o.getX()) return 1;
      if (x == o.getX()) {
         if (y > o.getY()) return 1;
         if (y < o.getY()) return -1;
         return 0;
      }
      return -1;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof Cell)) return false;
      return (this.getX() == ((Cell) o).getX() && this.getY() == ((Cell) o).getY());
   }

   @Override
   public String toString() {
      return "(" + x + "," + y + ")" + "->" + value;
   }
}
