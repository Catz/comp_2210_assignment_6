import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;

public class BoardTest {

   private final String[] DEFAULT_TEST_BOARD = new String[]{"E", "E", "C", "A", "A", "L", "E", "P", "H", "N", "B", "O", "Q", "T", "T", "Y"};

   @Test
   public void testConstructor_Square() {
      Board board = new Board();
      Assert.assertEquals(4, board.getSideLength());
   }

   @Test(expected=IllegalArgumentException.class)
   public void testConstructor_NonSquare() {
      Board board = new Board(Arrays.copyOf(DEFAULT_TEST_BOARD, DEFAULT_TEST_BOARD.length - 3));
   }

   @Test
   public void testGetCell_TopLeft() {
      Board board = new Board(DEFAULT_TEST_BOARD);
      Cell expected = new Cell(0,0,"");
      Cell actual = board.getCell(0,0);
      Assert.assertEquals(expected, actual);
   }

   @Test
   public void testGetCell_BottomRight() {
      Board board = new Board(DEFAULT_TEST_BOARD);
      Cell expected = new Cell(board.getSideLength() - 1, board.getSideLength() - 1, "");
      Cell actual = board.getCell(board.getSideLength() -1, board.getSideLength() -1);
      Assert.assertEquals(expected, actual);
   }

   @Test
   public void testGetCell_Center() {
      Board board = new Board(DEFAULT_TEST_BOARD);
      int half = board.getSideLength() / 2;
      Cell expected = new Cell(half, half, "");
      Cell results = board.getCell(half, half);
      Assert.assertEquals(expected, results);
   }

   @Test(expected=ArrayIndexOutOfBoundsException.class)
   public void testGetCell_OutOfBounds() {
      Board board = new Board(DEFAULT_TEST_BOARD);
      board.getCell(board.getSideLength(), board.getSideLength());
   }

   @Test
   public void testToString() throws Exception {
      Board board = new Board(DEFAULT_TEST_BOARD);
      String expected = "EECA\nALEP\nHNBO\nQTTY";
      String results = board.toString();
      Assert.assertEquals(expected, results);
   }
}